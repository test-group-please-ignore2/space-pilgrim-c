/*
    main.c - Space Pilgrim Main Loop
*/

/* C libraries */
#include <assert.h>
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

/* static definitions */
#define internal static
#define local_persist static
#define global_variable static

/* DEBUG - tutorial typedef shortcuts*/
typedef uint8_t     u8;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int32_t     i32;
typedef int64_t     i64;

/* my libraries */
#include "pt_console.c"
#include "game.c"
#include "fov.c"

/* constants */
#define GAME_VER        0.1
#define SCREEN_WIDTH    1280
#define SCREEN_HEIGHT   720
#define NUM_COLS        80
#define NUM_ROWS        45

bool can_move(Position pos) {
    // init move allowed bool
    bool moveAllowed = true;

    if ( // check if new position is outside the bounds of the screen
        (pos.x >= 0) && (pos.x <= NUM_COLS - 1) &&
        (pos.y >= 0) && (pos.y <= NUM_ROWS - 1)) {
        // loop through game objects
        for (uint32_t i = 0; i < MAX_GO; i++) {
            // get game object position
            Position p = positionComps[i];
            // check if game object intersects with new position
            if ((p.objectId != UNUSED) && (p.x == pos.x) && (p.y == pos.y)) {
                // check if game object blocks movement
                if (physicalComps[i].blocksMovement == true) {

                    moveAllowed = false;

                }

            }

        }
    // if new position is outside bounds of screen
    } else {

        moveAllowed = false;

    }

    return moveAllowed;

}

void render_screen(SDL_Renderer *renderer,
                   SDL_Texture *screen,
                   PT_Console *console) {

    PT_ConsoleClear(console);

    // loop over game objects
    for (uint32_t i = (MAX_GO - 1); i-- > 0;) {
        // check if game object has visibility data
        if (visibilityComps[i].objectId != UNUSED) {
            // get position data for game object
            Position *p = (Position *)game_object_get_component(
                &gameObjects[i],
                COMP_POSITION);

            // check for visibility
            if (fovMap[p -> x][p -> y] > 0) {

                PT_ConsolePutCharAt(
                    console,
                    visibilityComps[i].glyph,
                    p -> x, p -> y, 
                    visibilityComps[i].fgColor,
                    visibilityComps[i].bgColor);

            }

        }

    }

    SDL_UpdateTexture(screen, NULL, console -> pixels,
        SCREEN_WIDTH * sizeof(uint32_t));
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, screen, NULL, NULL);
    SDL_RenderPresent(renderer);

}

int main() {
    /* seed RNG */
    uint32_t seed = time(NULL);
    srand(seed);
    /* initiate SDL environment */
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow(
            "Space Pilgrim", // window title
            SDL_WINDOWPOS_UNDEFINED, // set window x-pos = undefined
            SDL_WINDOWPOS_UNDEFINED, // set window y-pos = undefined
            SCREEN_WIDTH, // set window width
            SCREEN_HEIGHT, // set window height
            0
        );

    SDL_Renderer *renderer = SDL_CreateRenderer(
            window, // SDL_Window object
            0, // figure out what this is
            SDL_RENDERER_SOFTWARE // set software rendering
        );

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT);

    // bucket of bits that write to the screen
    SDL_Texture *screen = SDL_CreateTexture( 
            renderer, // SDL_Renderer object
            SDL_PIXELFORMAT_RGBA8888, // set pixelformat = 32 bit pixels
            SDL_TEXTUREACCESS_STREAMING, // content optimization
            SCREEN_WIDTH, // set window width
            SCREEN_HEIGHT // set window height
        );

    PT_Console *console = PT_ConsoleInit(SCREEN_WIDTH, SCREEN_HEIGHT,
        NUM_ROWS, NUM_COLS);


    PT_ConsoleSetBitmapFont(console, "./terminal16x16.png", 0, 16, 16);

    world_state_init();

    // place player in a random room within the level
    GameObject *player = game_object_create();
    Visibility vis = {player -> id, '@', 0xFFFFFFFF, 0x000000FF};
    game_object_add_component(player, COMP_VISIBILITY, &vis);
    Physical phys = {player -> id, true, true};
    game_object_add_component(player, COMP_PHYSICAL, &phys);


    /* initialise world state */
    // create a level and put the player in it
    level_init(player);
    Position *playerPos = (Position *)game_object_get_component(
                                        player, 
                                        COMP_POSITION);

    fov_calculate(playerPos -> x, playerPos -> y, fovMap);


    /* game loop */
    // initialize exit condition
    bool done = false;
    bool recalculateFOV = false;

    while (!done) {

        SDL_Event event; // delare event type
        /* grab all events in event queue and process them */
        while (SDL_PollEvent(&event) != 0) {
            // exit function
            if (event.type == SDL_QUIT) {

                done = true;
                break;

            }

            if (event.type == SDL_KEYDOWN) {
                // handle keys
                SDL_Keycode key = event.key.keysym.sym;

                // init movement key check variable
                bool playerMove = false;

                // get player current position
                Position *playerPos = (Position *)game_object_get_component(
                    player,
                    COMP_POSITION);

                // initiate variable for new position
                Position newPos = {playerPos -> objectId,
                    playerPos -> x, playerPos -> y};

                switch (key) {
                    // handle game exit
                    case SDLK_ESCAPE:

                        done = true;
                        break;

                    // DEBUG - regenerate map
                    case SDLK_m:

                        level_init(player);
                        fov_calculate(playerPos -> x, playerPos -> y, fovMap);
                        break;
                    // DEBUG

                    // handle movement
                    case SDLK_KP_1:
                        // x - 1, y + 1
                        newPos.x -= 1;
                        newPos.y += 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_2:
                        // x = 0, y + 1
                        newPos.y += 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_3:
                        // x + 1, y + 1
                        newPos.x += 1;
                        newPos.y += 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_4:
                        // x - 1, y = 0
                        newPos.x -= 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_5:
                        // x = 0, y = 0
                        break;

                    case SDLK_KP_6:
                        // x + 1, y = 0
                        newPos.x += 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_7:
                        // x - 1, y - 1
                        newPos.x -= 1;
                        newPos.y -= 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_8:
                        // x = 0, y - 1
                        newPos.y -= 1;
                        playerMove = true;
                        break;

                    case SDLK_KP_9:
                        // x + 1, y - 1
                        newPos.x += 1;
                        newPos.y -= 1;
                        playerMove = true;
                        break;

                    default:

                        break;

                }

                // if movement key was pressed
                if (playerMove) {
                    // if player can move
                    if (can_move(newPos)) {
                        // move player
                        game_object_add_component(player,
                            COMP_POSITION, &newPos);

                        recalculateFOV = true;

                    }

                    playerMove = false;

                }

            }

        }

        if (recalculateFOV) {

            Position *playerPos = 
                    (Position *)game_object_get_component(
                    player, 
                    COMP_POSITION);

            fov_calculate(playerPos -> x, playerPos -> y, fovMap);
            recalculateFOV = false;

        }

        render_screen(renderer, screen, console);
    
    }

    /* gracefully exit window and SDL */
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;

}
