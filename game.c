/*
    game.c - Space Pilgrim Entity Stuff
*/

#define UNUSED 99999

typedef enum {

    COMP_POSITION = 0, // where is it
    COMP_VISIBILITY, // can you see it
    COMP_PHYSICAL, // is it actually in the game
    COMP_HEALTH, // can it be damaged
    COMP_MOVEMENT, // does it move

    /* define other components above this one */
    COMPONENT_COUNT // how many components are there

} GameComponent;

/* base entity definition */
typedef struct {

    uint32_t id;
    void *components[COMPONENT_COUNT];

} GameObject;

/* component definitions*/
typedef struct {

    uint32_t objectId;
    uint8_t x, y;

} Position;

typedef struct {

    uint32_t objectId;
    asciiChar glyph;
    uint32_t fgColor;
    uint32_t bgColor;

} Visibility;

typedef struct {

    uint32_t objectId;
    bool blocksMovement;
    bool blocksSight;

} Physical;

/* geometry definitions */
typedef struct {

    uint32_t x, y;

} Point;

/* world state */
#define MAX_GO 50000
global_variable GameObject gameObjects[MAX_GO];
global_variable Position positionComps[MAX_GO];
global_variable Visibility visibilityComps[MAX_GO];
global_variable Physical physicalComps[MAX_GO];

/* function declarations */
bool map_carve_room(uint32_t x, uint32_t y, uint32_t w, uint32_t h);
void map_carve_hallway_horz(Point from, Point to);
void map_carve_hallway_vert(Point from, Point to);
Point rect_random_point(PT_Rect rect);

/* world state init */
void world_state_init() {
    // loop through max game objects, set all IDs to UNUSED (-1)
    for (uint32_t i = 0; i < MAX_GO; i++) {
        gameObjects[i].id = UNUSED;
        positionComps[i].objectId = UNUSED;
        visibilityComps[i].objectId = UNUSED;
        physicalComps[i].objectId = UNUSED;
    }

}


/* game object management */
GameObject *game_object_create() {
    // find the next available object space
    GameObject *go = NULL;
    for (uint32_t i = 0; i < MAX_GO; i++) {

        if (gameObjects[i].id == UNUSED) {

            go = &gameObjects[i];
            go -> id = i;
            break;

        }

    }

    // make sure we're not exceeding max objects
    // assert(go != NULL);

    // null out any component references to previous components in that slot
    for (uint32_t i = 0; i < COMPONENT_COUNT; i++){

        go -> components[i] = NULL;

    }

    return go;

}

void game_object_add_component(GameObject *obj,
    GameComponent comp, void *compData) {

    assert(obj -> id != UNUSED);

    switch (comp) {

        // add position component to object
        case COMP_POSITION:; // don't forget the empty statement here
            // create variables for input data
            Position *pos = &positionComps[obj -> id];
            Position *posData = (Position *)compData;
            // put variables in bucket
            pos -> objectId = obj -> id;
            pos -> x = posData -> x;
            pos -> y = posData -> y;

            // give bucket to object
            obj -> components[comp] = pos;
            break;

        // add visibility component to object
        case COMP_VISIBILITY:; // don't forget the empty statement here
            // create variables for input data
            Visibility *vis = &visibilityComps[obj -> id];
            Visibility *visData = (Visibility *)compData;
            // put variables in bucket
            vis -> objectId = obj -> id;
            vis -> glyph = visData -> glyph;
            vis -> fgColor = visData -> fgColor;
            vis -> bgColor = visData -> bgColor;

            // give bucket to object
            obj -> components[comp] = vis;
            break;

        // add physical component to object
        case COMP_PHYSICAL:; // don't forget the empty statement here
            // create variables for input data
            Physical *phys = &physicalComps[obj -> id];
            Physical *physData = (Physical *)compData;
            // put variables in bucket
            phys -> objectId = obj -> id;
            phys -> blocksMovement = physData -> blocksMovement;
            phys -> blocksSight = physData -> blocksSight;

            // give bucket to object
            obj -> components[comp] = phys;
            break;

        default:
            assert(1 == 0);

    }

}

void game_object_destroy(GameObject *obj) {

    positionComps[obj -> id].objectId = UNUSED;
    visibilityComps[obj -> id].objectId = UNUSED;
    physicalComps[obj -> id].objectId = UNUSED;
    // TODO: clean up other components used by this objects

    obj -> id = UNUSED;

}

void *game_object_get_component(GameObject *obj, GameComponent comp) {

    return obj -> components[comp];

}

/* map management */

#define MAP_WIDTH   80
#define MAP_HEIGHT  39
#define MAX_ROOMS   10
#define ROOM_SIZE   12 

// define map cell array, true = filled
bool mapCells[MAP_WIDTH][MAP_HEIGHT];

void map_generate() {
    // mark all cells as filled
    for (uint32_t x = 0; x < MAP_WIDTH; x++) {
     
        for (uint32_t y = 0; y < MAP_HEIGHT; y++) {

            mapCells[x][y] = true;

        }

    }

    PT_Rect rooms[MAX_ROOMS];
    bool roomsDone = false;
    uint32_t cellsUsed = 0;
    uint32_t roomCount = 0;

    // room generator
    while (!roomsDone) {
        // generate size of room
        uint32_t w = (rand() % ROOM_SIZE) + 3;
        uint32_t h = (rand() % ROOM_SIZE) + 3;

        // generate position of room
        uint32_t x = rand() % (MAP_WIDTH - w - 1);
        uint32_t y = rand() % (MAP_HEIGHT - h - 1);
        if (x == 0) x = 1;
        if (y == 0) y = 1;

        bool success = map_carve_room(x, y, w, h);

        if (success) {

            PT_Rect r = {x, y, w, h};
            rooms[roomCount] = r;
            roomCount += 1;
            cellsUsed += (w * h);

        }

        // exit if suitable percentage of cells are filled
        if (roomCount == MAX_ROOMS) {

            roomsDone = true;

        }

    }

    // join all rooms with corridors, so that all rooms are accessible
    for (uint32_t r = 1; r < roomCount; r++) {

        PT_Rect from = rooms[r - 1];
        PT_Rect to = rooms[r];
        Point fromPt = rect_random_point(from);
        Point toPt = rect_random_point(to);

        if (rand() % 2 == 0) {
            // move across then up
            Point midPt = {toPt.x, fromPt.y};
            map_carve_hallway_horz(fromPt, midPt);
            map_carve_hallway_vert(midPt , toPt);

        } else {
            // move up then across
            Point midPt = {fromPt.x, toPt.y};
            map_carve_hallway_vert(fromPt, midPt);
            map_carve_hallway_horz(midPt , toPt);

        }

    }

}

bool map_carve_room(uint32_t x, uint32_t y, uint32_t w, uint32_t h) {
    // determine if any cells within the given rectangle are filled
    for (uint8_t i = (x - 1); i < (x + w + 1); i++) {

        for (uint8_t j = (y - 1); j < (y + h + 1); j++) {

            if (mapCells[i][j] == false) {

                return false;

            }

        }

    }

    // carve the room
    for (uint8_t i = x; i < (x + w); i++) {

        for (uint8_t j = y; j < (y + h); j++) {

            mapCells[i][j] = false;

        }

    }
    
    return true;

}

Point rect_random_point(PT_Rect rect) {

    uint32_t px = (rand () % rect.w) + rect.x;
    uint32_t py = (rand () % rect.h) + rect.y;

    Point ret = {px, py};

    return ret;

}

void map_carve_hallway_horz(Point from, Point to) {

    uint32_t first, last;

    if (from.x < to.x) {

        first = from.x;
        last = to.x;

    } else {

        first = to.x;
        last = from.x;

    }

    for (uint32_t x = first; x <= last; x++) {

        mapCells[x][from.y] = false;

    }

}

void map_carve_hallway_vert(Point from, Point to) {

    uint32_t first, last;

    if (from.y < to.y) {

        first = from.y;
        last = to.y;

    } else {

        first = to.y;
        last = from.y;

    }

    for (uint32_t y = first; y <= last; y++) {

        mapCells[from.x][y] = false;

    }

}

void add_wall(uint8_t x,uint8_t y) {

    GameObject *wall = game_object_create();
    Position pos = {wall -> id, x, y};
    game_object_add_component(wall, COMP_POSITION, &pos);
    Visibility vis = {wall -> id, '#', 0x2E4C5DFF, 0x000000FF};
    game_object_add_component(wall, COMP_VISIBILITY, &vis);
    Physical wallPhys = {wall -> id, true, true};
    game_object_add_component(wall, COMP_PHYSICAL, &wallPhys);

}

/* level management */
void level_init(GameObject *player) {
    // clear the previous level data from the world state
    for (uint32_t i = 0; i < MAX_GO; i++) {
        // destroy everything that isn't a player
        if ((gameObjects[i].id != player -> id) &&
            (gameObjects[i].id != UNUSED)) {

            game_object_destroy(&gameObjects[i]);

        }

    }

    // what it says on the box
    map_generate();

    // add walls
    for (uint32_t x = 0; x < MAP_WIDTH; x++) {
     
        for (uint32_t y = 0; y < MAP_HEIGHT; y++) {

            if (mapCells[x][y]) {
                add_wall(x, y);

            }

        }

    }

    for (;;) {

        uint32_t x = rand() % MAP_WIDTH;
        uint32_t y = rand() % MAP_HEIGHT;

        if (mapCells[x][y] == false) {

            Position pos = {player -> id, x, y};
            game_object_add_component(player, COMP_POSITION, &pos);
            break;

        } 

    }

}