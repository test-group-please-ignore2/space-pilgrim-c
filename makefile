OBJS = main.c

LINUX_CC = gcc

COMPILER_FLAGS = -Wall -g `sdl2-config --cflags`

LINKER_FLAGS = `sdl2-config --libs` -lm

LINUX_NAME = space-pilgrim-c

all: $(OBJS)
	$(LINUX_CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(LINUX_NAME)