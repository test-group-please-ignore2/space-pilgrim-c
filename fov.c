/*
    fov.c - field of vision stuff
*/

#define FOV_DISTANCE    5

// allocate memory for the fov map
global_variable uint32_t fovMap[MAP_WIDTH][MAP_HEIGHT];

void fov_calculate(uint32_t x, uint32_t y, uint32_t fovMap[][MAP_HEIGHT]) {

    // reset FOV to default state
    for (uint32_t x = 0; x < MAP_WIDTH; x++) {

        for (uint32_t y = 0; y < MAP_HEIGHT; y++) {

            fovMap[x][y] = 0;

        }

    }

    /* cast visibility out in four directions to determine rectangle */
    // cast x
    uint32_t x1 = 0;
    
    if (x >= FOV_DISTANCE) {x1 = x - FOV_DISTANCE;}

    uint32_t x2 = x + FOV_DISTANCE;

    if (x2 >= MAP_WIDTH) {x2 = MAP_WIDTH - 1;}
    
    // cast y
    uint32_t y1 = 0;

    if (y >= FOV_DISTANCE) {y1 = y - FOV_DISTANCE;}

    uint32_t y2 = y + FOV_DISTANCE;

    if (y2 >= MAP_HEIGHT) {y2 = MAP_HEIGHT - 1;}

    /* apply visibility to FOV map */
    for (uint32_t fx = x1; fx <= x2; fx++) {

        for (uint32_t fy = y1; fy <= y2; fy++) {

            fovMap[fx][fy] = 10;

        }

    }

}